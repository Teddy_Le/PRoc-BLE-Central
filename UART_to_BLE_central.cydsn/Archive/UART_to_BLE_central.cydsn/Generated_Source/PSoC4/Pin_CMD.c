/*******************************************************************************
* File Name: Pin_CMD.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "Pin_CMD.h"

#define SetP4PinDriveMode(shift, mode)  \
    do { \
        Pin_CMD_PC =   (Pin_CMD_PC & \
                                (uint32)(~(uint32)(Pin_CMD_DRIVE_MODE_IND_MASK << (Pin_CMD_DRIVE_MODE_BITS * (shift))))) | \
                                (uint32)((uint32)(mode) << (Pin_CMD_DRIVE_MODE_BITS * (shift))); \
    } while (0)


/*******************************************************************************
* Function Name: Pin_CMD_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void Pin_CMD_Write(uint8 value) 
{
    uint8 drVal = (uint8)(Pin_CMD_DR & (uint8)(~Pin_CMD_MASK));
    drVal = (drVal | ((uint8)(value << Pin_CMD_SHIFT) & Pin_CMD_MASK));
    Pin_CMD_DR = (uint32)drVal;
}


/*******************************************************************************
* Function Name: Pin_CMD_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  Pin_CMD_DM_STRONG     Strong Drive 
*  Pin_CMD_DM_OD_HI      Open Drain, Drives High 
*  Pin_CMD_DM_OD_LO      Open Drain, Drives Low 
*  Pin_CMD_DM_RES_UP     Resistive Pull Up 
*  Pin_CMD_DM_RES_DWN    Resistive Pull Down 
*  Pin_CMD_DM_RES_UPDWN  Resistive Pull Up/Down 
*  Pin_CMD_DM_DIG_HIZ    High Impedance Digital 
*  Pin_CMD_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void Pin_CMD_SetDriveMode(uint8 mode) 
{
	SetP4PinDriveMode(Pin_CMD__0__SHIFT, mode);
}


/*******************************************************************************
* Function Name: Pin_CMD_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro Pin_CMD_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 Pin_CMD_Read(void) 
{
    return (uint8)((Pin_CMD_PS & Pin_CMD_MASK) >> Pin_CMD_SHIFT);
}


/*******************************************************************************
* Function Name: Pin_CMD_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 Pin_CMD_ReadDataReg(void) 
{
    return (uint8)((Pin_CMD_DR & Pin_CMD_MASK) >> Pin_CMD_SHIFT);
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(Pin_CMD_INTSTAT) 

    /*******************************************************************************
    * Function Name: Pin_CMD_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 Pin_CMD_ClearInterrupt(void) 
    {
		uint8 maskedStatus = (uint8)(Pin_CMD_INTSTAT & Pin_CMD_MASK);
		Pin_CMD_INTSTAT = maskedStatus;
        return maskedStatus >> Pin_CMD_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
